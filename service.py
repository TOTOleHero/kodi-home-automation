################################################################################
#
# xPL advanced
#
# Version 1.0
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# Linking this library statically or dynamically with other modules is
# making a combined work based on this library. Thus, the terms and
# conditions of the GNU General Public License cover the whole
# combination.
#
################################################################################

#TODO Add ability to activate or not automatic with a XPL order
# TODO : fix specific problem with Windows, when screensaver is activated, if the screen is turned off, then the screensaver is immediately deactivated !

import sys, string, select, threading, os.path
import xbmc
import xbmcaddon
import simplejson
import re
import urllib
import time
from socket import *
from xpl import *

__addon_id__= 'service.xpl.automation'
utils = xbmcaddon.Addon(__addon_id__)

class Screensaver(xbmc.Monitor) :
	print("DLMGR: Checking PlayBackState")

	source = "utopie-kodi." + gethostname().replace("-", "").split(".")[0][:16]
	xpl = Xpl(source, xbmc.getIPAddress())

	def __init__ (self):
		xbmc.Monitor.__init__(self)
		self.justAsleep=False
		
	def onScreensaverActivated( self ):
		xbmc.log("Screensaver activated")
		self.screensaver_active = True
		if (self.justAsleep == False):
# 			xbmc.log("First call, we can go to sleep")
			# real activation of screensaver
			if (not xbmc.getCondVisibility('Player.HasAudio')):
				#self.justAsleep = True
				self.sleeping()
				#time.sleep(3)
				#xbmc.log("Now if we receive a wake up, it will be a real one !")
				#self.justAsleep = False
# 		else:
# 			xbmc.log("Second call, Nothing to do we are already sleeping")
# 			# Out own activation, we just change the state af justAsleep to be ready for real wakeup
# 			self.justAsleep = False

	def onScreensaverDeactivated( self ):
		xbmc.log("Screensaver deactivated")
		self.screensaver_active = False
		if (self.justAsleep==False):
# 			xbmc.log("Was not just asleep we must wake up !")
			self.waking()
# 		else:
# 			# We need to be sure we did not wake up Kodi (Windows stupid HDMI feature)
# 			xbmc.log("We were just asleep, false alarm probably we go back to sleep")
# 			xbmc.executebuiltin("ActivateScreensaver")

	def sleeping(self):
		xbmc.log("Sleeping asked")
		if (utils.getSetting("ActivatedForScreensaver") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*","media.mptrnspt", "mp=xbmc\ncommand=sleeping")
		if (utils.getSetting("ActivatedForScreensaverXplAction1") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("ScreensaverXplAction1Class"), utils.getSetting("ScreensaverXplAction1Params").replace("&", "\n"))
		if (utils.getSetting("ActivatedForScreensaverXplAction2") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("ScreensaverXplAction2Class"), utils.getSetting("ScreensaverXplAction2Params").replace("&", "\n"))
		if (utils.getSetting("ActivatedForScreensaverXplAction3") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("ScreensaverXplAction3Class"), utils.getSetting("ScreensaverXplAction3Params").replace("&", "\n"))
		if (utils.getSetting("ActivatedForScreensaverIPAction1") == "true"):
			urllib.urlopen(utils.getSetting("ScreensaverIPAction1Url"))
		if (utils.getSetting("ActivatedForScreensaverIPAction2") == "true"):
			urllib.urlopen(utils.getSetting("ScreensaverIPAction2Url"))
		if (utils.getSetting("ActivatedForScreensaverIPAction3") == "true"):
			urllib.urlopen(utils.getSetting("ScreensaverIPAction3Url"))
		
	def waking(self):
		xbmc.log("Waking asked")
		if (utils.getSetting("ActivatedForWakeup") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*","media.mptrnspt", "mp=xbmc\ncommand=waking")
		if (utils.getSetting("ActivatedForWakeupXplAction1") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("WakeupXplAction1Class"), utils.getSetting("WakeupXplAction1Params").replace("&", "\n"))
		if (utils.getSetting("ActivatedForWakeupXplAction2") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("WakeupXplAction2Class"), utils.getSetting("WakeupXplAction2Params").replace("&", "\n"))
		if (utils.getSetting("ActivatedForWakeupXplAction3") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("WakeupXplAction3Class"), utils.getSetting("WakeupXplAction3Params").replace("&", "\n"))
		if (utils.getSetting("ActivatedForWakeupIPAction1") == "true"):
			urllib.urlopen(utils.getSetting("WakeupIPAction1Url"))
		if (utils.getSetting("ActivatedForWakeupIPAction2") == "true"):
			urllib.urlopen(utils.getSetting("WakeupIPAction2Url"))
		if (utils.getSetting("ActivatedForWakeupIPAction3") == "true"):
			urllib.urlopen(utils.getSetting("WakeupIPAction3Url"))
		

class XplPlayer( xbmc.Player ) :
# xPL source name

	source = "utopie-kodi." + gethostname().replace("-", "").split(".")[0][:16]
	xpl = Xpl(source, xbmc.getIPAddress())
	lastState = ""
	lastKind = ""
	lastAudioTag = None
	lastVideoTag = None
	
	def __init__ ( self ):
		xbmc.Player.__init__( self )
		self.xpl.parse = self.parseBroadcast
		self.currentlyPlaying = "stop"
		self.currentGenre = ""
		self.token = False
		xbmc.log("FA: init completed")

	def onPlayBackStarted(self):
		xbmc.log("XBMC play");
		# This token is used to avoid reentry in this function twice for the same event
		if self.token == True:
			xbmc.log("Token already taken, avoiding to mix up")
		else:
			self.token = True
			
			if xbmc.Player().isPlayingAudio():
				xbmc.log("Is playing audio")
				xbmc.log("Checking if movie was on");
				if (self.currentlyPlaying == "movie"):
					self.movieStop()
				xbmc.log("Checking if episode was on");
				if (self.currentlyPlaying == "episode"):
					self.tvShowStop()
	
				tag = xbmc.Player().getMusicInfoTag();
				if not tag.getGenre():
					xbmc.log("No genre detected, trying again...")
					xbmc.sleep(1000)
					tag = xbmc.Player().getMusicInfoTag();
					if not tag.getGenre():
						xbmc.log("NO GENRE DETECTED !!!")
				
				# Already playing music, but genre is changed
				xbmc.log("Checking if music was on");
				if (self.currentlyPlaying == "music"):			
					xbmc.log("As music was on, we need to ckeck if a genre was on");
					if (utils.getSetting("ActivatedForMusic1Stop") == "true"):
						if re.search("(?i)" + utils.getSetting("Music1Genre"), self.currentGenre) and not re.search("(?i)" + utils.getSetting("Music1Genre"), tag.getGenre()):
							xbmc.log("Genre 1 was on, stopping it");
							self.music1Stop()
					if (utils.getSetting("ActivatedForMusic2Stop") == "true"):
						if re.search("(?i)" + utils.getSetting("Music2Genre"), self.currentGenre) and not re.search("(?i)" + utils.getSetting("Music2Genre"), tag.getGenre()):
							xbmc.log("Genre 2 was on, stopping it");
							self.music2Stop()
					if (utils.getSetting("ActivatedForMusic3Stop") == "true"):
						if re.search("(?i)" + utils.getSetting("Music3Genre"), self.currentGenre) and not re.search("(?i)" + utils.getSetting("Music3Genre"), tag.getGenre()):
							xbmc.log("Genre 3 was on, stopping it");
							self.music3Stop()
				else:
					if (utils.getSetting("ActivatedForMusic0Start") == "true"):
						xbmc.log("Music is active and wasn't before, starting what we need");					
						self.music0Start()
		
				#kind = "audio"
				#if kind != self.lastKind:
					#self.lastState = "stop"
					
					#self.xpl.sendBroadcast("xpl-trig", "*","media.mptrnspt", "mp=xbmc\ncommand=stop\nkind=" + self.lastKind)
			
				
				#if self.lastAudioTag is None or self.lastAudioTag.getTitle() != tag.getTitle() or self.lastAudioTag.getArtist() != tag.getArtist() or self.lastAudioTag.getAlbum() != tag.getAlbum():
				xbmc.log("Try to send broadcast")
				media = "mp=xbmc\n"
				media = media + "kind=audio\n"
				media = media + "title=" + tag.getTitle() + "\n"
				media = media + "genre=" + tag.getGenre() + "\n"
				media = media + "album=" + tag.getAlbum() + "\n"
				media = media + "artist=" + tag.getArtist() + "\n"
				media = media + "duration=" + str(xbmc.Player().getTotalTime()) + "\n"
				self.xpl.sendBroadcast("xpl-trig", "*","media.mpmedia", media)
				self.lastAudioTag = tag
				self.currentlyPlaying = "music"
				if (utils.getSetting("ActivatedForMusic1Start") == "true"):
					if re.search("(?i)" + utils.getSetting("Music1Genre"), tag.getGenre()) and not re.search("(?i)" + utils.getSetting("Music1Genre"), self.currentGenre):
						xbmc.log("Genre 1 is active, starting it");					
						self.music1Start()
				if (utils.getSetting("ActivatedForMusic2Start") == "true"):
					if re.search("(?i)" + utils.getSetting("Music2Genre"), tag.getGenre()) and not re.search("(?i)" + utils.getSetting("Music2Genre"), self.currentGenre):
						xbmc.log("Genre 2 is active, starting it");					
						self.music2Start()
				if (utils.getSetting("ActivatedForMusic3Start") == "true"):
					if re.search("(?i)" + utils.getSetting("Music3Genre"), tag.getGenre()) and not re.search("(?i)" + utils.getSetting("Music3Genre"), self.currentGenre):
						xbmc.log("Genre 3 is active, starting it");					
						self.music3Start()
				self.currentGenre = tag.getGenre();
				
			if xbmc.Player().isPlayingVideo():
				if (self.currentlyPlaying == "music"):			
					if (utils.getSetting("ActivatedForMusic1Stop") == "true"):
						if re.search("(?i)" + utils.getSetting("Music1Genre"), self.currentGenre):
							self.music1Stop()
					if (utils.getSetting("ActivatedForMusic2Stop") == "true"):
						if re.search("(?i)" + utils.getSetting("Music2Genre"), self.currentGenre):
							self.music2Stop()
					if (utils.getSetting("ActivatedForMusic3Stop") == "true"):
						if re.search("(?i)" + utils.getSetting("Music3Genre"), self.currentGenre):
							self.music3Stop()
				#kind = "video"
	# 			if kind != self.lastKind:
	# 				self.lastState = "stop"
	# 				self.xpl.sendBroadcast("xpl-trig", "*","media.mptrnspt", "mp=xbmc\ncommand=stop\nkind=" + self.lastKind)
	 		
				tag = xbmc.Player().getVideoInfoTag();
				_json_query = xbmc.executeJSONRPC('{"jsonrpc": "2.0", "method": "Player.GetItem", "params": { "properties": ["title", "season", "episode", "duration", "showtitle"], "playerid": 1 }, "id": "VideoGetItem"}')
				_json_query = unicode(_json_query, 'utf-8', errors='ignore')
				xbmc.log(_json_query)
				_json_pl_response = simplejson.loads(_json_query)
				_type = _json_pl_response.get( "result", {} ).get( "item" ).get( "type" )
				_title = _json_pl_response.get( "result", {} ).get( "item" ).get( "title" )
				
				media = "mp=xbmc\n"
				media = media + "kind=video\n"
				media = media + "type=" + _type + "\n"
				media = media + "title=" + _title + "\n"
				media = media + "duration=" + str(xbmc.Player().getTotalTime()) + "\n"
				#xbmc.log('\n'.join(dir(tag)))
				self.xpl.sendBroadcast("xpl-trig", "*","media.mpmedia", media)
				self.lastVideoTag = tag
				self.currentlyPlaying = _type
				if (_type == "movie"):
					self.movieStart()
				if (_type == "episode"):
					self.tvshowStart()
		self.token = False

	def onPlayBackEnded(self):
		xbmc.log("XBMC ended");
		if (self.currentlyPlaying == "movie"):
			self.movieStop()
		if (self.currentlyPlaying == "episode"):
			self.tvShowStop()
		if (self.currentlyPlaying == "music"):
			if (utils.getSetting("ActivatedForMusic0Stop") == "true"):
				self.music0Stop()
			if (utils.getSetting("ActivatedForMusic1Stop") == "true"):
				if re.search("(?i)" + utils.getSetting("Music1Genre"), self.currentGenre):
					self.music1Stop()
			if (utils.getSetting("ActivatedForMusic2Stop") == "true"):
				if re.search("(?i)" + utils.getSetting("Music2Genre"), self.currentGenre):
					self.music2Stop()
			if (utils.getSetting("ActivatedForMusic3Stop") == "true"):
				if re.search("(?i)" + utils.getSetting("Music3Genre"), self.currentGenre):
					self.music3Stop()

		self.currentlyPlaying = "stop"
		self.currentGenre = ""
# 		if self.screensaver_active:
# 			self.sleeping()

	def onPlayBackStopped(self):
		xbmc.log("XBMC stop");
		if (self.currentlyPlaying == "movie"):
			self.movieStop()
		if (self.currentlyPlaying == "episode"):
			self.tvShowStop()
		if (self.currentlyPlaying == "music"):
			if (utils.getSetting("ActivatedForMusic0Stop") == "true"):
				self.music0Stop()
			if (utils.getSetting("ActivatedForMusic1Stop") == "true"):
				if re.search("(?i)" + utils.getSetting("Music1Genre"), self.currentGenre):
					self.music1Stop()
			if (utils.getSetting("ActivatedForMusic2Stop") == "true"):
				if re.search("(?i)" + utils.getSetting("Music2Genre"), self.currentGenre):
					self.music2Stop()
			if (utils.getSetting("ActivatedForMusic3Stop") == "true"):
				if re.search("(?i)" + utils.getSetting("Music3Genre"), self.currentGenre):
					self.music3Stop()
		self.currentlyPlaying = "stop"
		self.currentGenre = ""
# 		if self.screensaver_active:
# 			self.sleeping()
			
	def onPlayBackPaused(self):
		xbmc.log("XBMC pause");
		if (self.currentlyPlaying == "movie"):
			self.moviePause()
		if (self.currentlyPlaying == "episode"):
			self.tvShowPause()
		if (self.currentlyPlaying == "music"):
			if (utils.getSetting("ActivatedForMusic0Pause") == "true"):
				self.music0Pause()
			if (utils.getSetting("ActivatedForMusic1Pause") == "true"):
				if re.search("(?i)" + utils.getSetting("Music1Genre"), self.currentGenre):
					self.music1Pause()
			if (utils.getSetting("ActivatedForMusic2Pause") == "true"):
				if re.search("(?i)" + utils.getSetting("Music2Genre"), self.currentGenre):
					self.music2Pause()
			if (utils.getSetting("ActivatedForMusic3Pause") == "true"):
				if re.search("(?i)" + utils.getSetting("Music3Genre"), self.currentGenre):
					self.music3Pause()
		
	def onPlayBackResumed(self):
		xbmc.log("Resumed");
		if (self.currentlyPlaying == "movie"):
			self.movieStart()
		if (self.currentlyPlaying == "episode"):
			self.tvshowStart()
		if (self.currentlyPlaying == "music"):
			if (utils.getSetting("ActivatedForMusic0Start") == "true"):
				self.music0Start()
			if (utils.getSetting("ActivatedForMusic1Start") == "true"):
				if re.search("(?i)" + utils.getSetting("Music1Genre"), self.currentGenre):
					self.music1Start()
			if (utils.getSetting("ActivatedForMusic2Start") == "true"):
				if re.search("(?i)" + utils.getSetting("Music2Genre"), self.currentGenre):
					self.music2Start()
			if (utils.getSetting("ActivatedForMusic3Start") == "true"):
				if re.search("(?i)" + utils.getSetting("Music3Genre"), self.currentGenre):
					self.music3Start()
					
					
	def default(self, object):
		xbmc.log(object)
		if (utils.getSetting("ActivatedFor" + object + "XplAction1") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("" + object + "XplAction1Class"), utils.getSetting("" + object + "XplAction1Params").replace("&", "\n"))
			xbmc.sleep(int(utils.getSetting("DelayBetweenIPOrders")))
		if (utils.getSetting("ActivatedFor" + object + "XplAction2") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("" + object + "XplAction2Class"), utils.getSetting("" + object + "XplAction2Params").replace("&", "\n"))
			xbmc.sleep(int(utils.getSetting("DelayBetweenIPOrders")))
		if (utils.getSetting("ActivatedFor" + object + "XplAction3") == "true"):
			self.xpl.sendBroadcast("xpl-cmnd", "*",utils.getSetting("" + object + "XplAction3Class"), utils.getSetting("" + object + "XplAction3Params").replace("&", "\n"))
		if (utils.getSetting("ActivatedFor" + object + "IPAction1") == "true"):
			xbmc.log("calling url " + utils.getSetting("" + object + "IPAction1Url"))
			urllib.urlopen(utils.getSetting("" + object + "IPAction1Url"))
			xbmc.sleep(int(utils.getSetting("DelayBetweenIPOrders")))
		if (utils.getSetting("ActivatedFor" + object + "IPAction2") == "true"):
			urllib.urlopen(utils.getSetting("" + object + "IPAction2Url"))
			xbmc.sleep(int(utils.getSetting("DelayBetweenIPOrders")))
		if (utils.getSetting("ActivatedFor" + object + "IPAction3") == "true"):
			urllib.urlopen(utils.getSetting("" + object + "IPAction3Url"))
		
	def movieStart(self):
		self.default("MovieStart")

	def tvshowStart(self):
		self.default("TVShowStart")
		
	def music0Start(self):		
 		xbmc.executebuiltin('XBMC.SetVolume('+utils.getSetting("Music0Volume")+')')
		self.default("Music0Start")
		
	def music1Start(self):
 		xbmc.executebuiltin('XBMC.SetVolume('+utils.getSetting("Music1Volume")+')')
		self.default("Music1Start")

	def music2Start(self):
 		xbmc.executebuiltin('XBMC.SetVolume('+utils.getSetting("Music2Volume")+')')
		self.default("Music2Start")

	def music3Start(self):
 		xbmc.executebuiltin('XBMC.SetVolume('+utils.getSetting("Music3Volume")+')')
		self.default("Music3Start")

	def movieStop(self):
		self.default("MovieStop")
		
	def tvShowStop(self):
		self.default("TVShowStop")

	def music0Stop(self):
		self.default("Music0Stop")

	def music1Stop(self):
		self.default("Music1Stop")

	def music2Stop(self):
		self.default("Music2Stop")

	def music3Stop(self):
		self.default("Music3Stop")

	def moviePause(self):
		self.default("MoviePause")
		
	def tvShowPause(self):
		self.default("TVShowPause")

	def music0Pause(self):
		self.default("Music0Pause")

	def music1Pause(self):
		self.default("Music1Pause")

	def music2Pause(self):
		self.default("Music2Pause")

	def music3Pause(self):
		self.default("Music3Pause")

	

	def parseBroadcast(self, data):
		#xbmc.log("FA, data:" + data)
		parts = data.split("\n")
		msgtype = parts[0].lower()
		offset = 2
		values = dict()
		if parts[offset-1] == "{":
			while parts[offset] != "}":
				part = parts[offset]
				if part != "}":
					value=part.split("=")
					if len(value) == 2:
						values[value[0].lower()]=value[1]
					offset = offset + 1
				else:
					break
		offset = offset + 1
		schema = parts[offset].lower()
		offset = offset + 2
		if parts[offset-1] == "{":
			while parts[offset] != "}":
				part = parts[offset]
				if part != "}":
					value=part.split("=")
					if len(value) == 2:
						values[value[0].lower()]=value[1]
					offset = offset + 1
				else:
					break
				
		if msgtype != "xpl-stat":
			temp = dict(values)
			del temp['source']
			del temp['target']
			del temp['hop']
# 			utils.setSetting("InvisibleParameterClass", schema)
# 			xbmc.log("setting InvisibleParameterClass to " + schema)
# 			xbmc.log("Finally we got " + utils.getSetting("InvisibleParameterClass"))

# 			utils.setSetting("InvisibleParameterParameter", '&'.join("%s=%r" % (key,val) for (key,val) in temp.iteritems()))
# 			xbmc.log("setting InvisibleParameterParameter to " + '&'.join("%s=%r" % (key,val) for (key,val) in temp.iteritems()))
# 			xbmc.log("Finally we got " + utils.getSetting("InvisibleParameterParameter"))
			#utils.saveSettings()
		
		if (values['target'] != "*" and values['target'] != "utopie-kodi.*" and values['target'] != self.source) or values['source'] == self.source:
			return
	
		if msgtype == "xpl-cmnd":
			
			if schema =="osd.basic":
				if( values['command'].lower() == "write" or values['command'].lower() == "clear"):
					if( values['text'] ):
						displayTime = values.get('delay', "20")
						self.osdMessage("xPL Message", values['text'].replace("\\n", "\n"), displayTime, "")
			
			if schema =="cid.basic" or schema =="cid.netcall" or schema =="cid.meteor" :
				if( values['calltype'].lower() == "inbound"):
					cln = values['cln']
					if( len(cln) == 0 ):
						cln = "Incoming\x201A call"
					self.osdMessage(cln, values['phone'], 20, "phone")	
			
			
			if schema == "media.basic":
				print "Got media command:" + values['command']
				if values['command'].lower() == "stop":
					xbmc.executebuiltin('PlayerControl(Stop)')
				if values['command'].lower() == "play":
					xbmc.executebuiltin('PlayerControl(Play)')
				if values['command'].lower() == "pause":
					if xbmc.Player().isPlaying():
						xbmc.executebuiltin('PlayerControl(Play)')
				if values['command'].lower() == "skip":
					xbmc.executebuiltin('PlayerControl(Next)')
					
			if schema == "media.request":
				if values['request'].lower() == "devinfo":
					self.xpl.sendBroadcast("xpl-stat", values['source'],"media.devinfo", "name=Boxee xPL Interface\nversion=1.0\nauthor=Sam Steele\ninfo-url=http://www.c99.org/\nmp-list=boxee\n")
				
				if values['request'].lower() == "devstate":
					self.xpl.sendBroadcast("xpl-stat", values['source'],"media.devstate", "power=on\nconnected=true\n")
	
				if values['request'].lower() == "mpinfo":
					self.xpl.sendBroadcast("xpl-stat", values['source'],"media.mpinfo", "mp=xbmc\nname=Boxee\ncommand-list=play,stop,pause,skip\naudio=true\nvideo=true\n")
	
				if values['request'].lower() == "mptrnspt":
					self.xpl.sendBroadcast("xpl-stat", values['source'],"media.mptrnspt", "mp=xbmc\ncommand="+self.lastState+"\n")
	
				if values['request'].lower() == "mpmedia":
					media = "mp=xbmc\n"
					if xbmc.Player().isPlaying():
						if xbmc.Player().isPlayingAudio():
							tag = xbmc.Player().getMusicInfoTag();
							media = "mp=xbmc\n"
							media = media + "kind=audio\n"
							media = media + "title=" + tag.getTitle() + "\n"
							media = media + "album=" + tag.getAlbum() + "\n"
							media = media + "artist=" + tag.getArtist() + "\n"
							media = media + "duration=" + str(xbmc.Player().getTotalTime()) + "\n"
							media = media + "format=" + os.path.splitext(xbmc.Player().getPlayingFile())[1][1:] + "\n"
						else:
							tag = xbmc.Player().getVideoInfoTag();
							media = "mp=xbmc\n"
							media = media + "kind=video\n"
							media = media + "title=" + tag.getTitle() + "\n"
							media = media + "album=" + "\n"
							media = media + "artist=" + "\n"
							media = media + "duration=" + str(xbmc.Player().getTotalTime()) + "\n"
							media = media + "format=" + os.path.splitext(xbmc.Player().getPlayingFile())[1][1:] + "\n"
							
					self.xpl.sendBroadcast("xpl-stat", values['source'],"media.mpmedia", media)


	def osdMessage(self, title, message, displayTimeSeconds, icon):
		#todo: messages containing , or . will screw stuff up. Must find a way to fix
		executeString = "Notification(%s,%s,%s,%s)" % ( title, message ,str(displayTimeSeconds) + "000", icon )
		
		xbmc.log("FA:executeString=" + executeString)
		xbmc.executebuiltin(executeString)
		
	def destroy(self):
		self.xpl.stop()
		
player=XplPlayer()
screensaver=Screensaver()
while True:
	if screensaver.waitForAbort(30):
		# Abort was requested while waiting. We should exit
		break
	
xbmc.log("Destruction requested. Stopping plugin") 
player.destroy()


