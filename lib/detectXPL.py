#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#     Copyright (C) 2011-2013 Martijn Kaijser
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

#import modules
import os
import sys
import xbmc
import xbmcgui
import xbmcaddon
#from xbmcaddon import Addon
from xpl import *

### get addon info
__addon_id__= 'service.xpl.automation'
utils = xbmcaddon.Addon(__addon_id__)
__addonname__ = utils.getAddonInfo("name")

source = "utopie-kodi." + gethostname().replace("-", "").split(".")[0][:16]
xpl = Xpl(source, xbmc.getIPAddress())
dialog = xbmcgui.DialogProgress()
num = 0
# class DetectXPL:
#     # constants
#     WINDOW = 10147
#     CONTROL_LABEL = 1
#     CONTROL_TEXTBOX = 5
# 
#     def __init__(self, *args, **kwargs):
#         xbmc.Player.__init__( self )
#         # activate the text viewer window
# #         xbmc.executebuiltin("ActivateWindow(%d)" % (self.WINDOW,))
#         # get window
# #         self.window = xbmcgui.Window(self.WINDOW)
#         # give window time to initialize
#         xbmc.sleep(100)
#         source = "utopie-kodi." + gethostname().replace("-", "").split(".")[0][:16]
#         xpl = Xpl(source, xbmc.getIPAddress())
#         xpl.parse = self.parseBroadcast
#         # set controls
# #         self.setControls()
# 
def parseBroadcast(data):
    xbmc.log("FA, data:" + data)
    parts = data.split("\n")
    msgtype = parts[0].lower()
    offset = 2
    values = dict()
    if parts[offset-1] == "{":
        while parts[offset] != "}":
            part = parts[offset]
            if part != "}":
                value=part.split("=")
                if len(value) == 2:
                    values[value[0].lower()]=value[1]
                offset = offset + 1
            else:
                break
    offset = offset + 1
    schema = parts[offset].lower()
    offset = offset + 2
    if parts[offset-1] == "{":
        while parts[offset] != "}":
            part = parts[offset]
            if part != "}":
                value=part.split("=")
                if len(value) == 2:
                    values[value[0].lower()]=value[1]
                offset = offset + 1
            else:
                break
            
    if msgtype != "xpl-stat":
        temp = dict(values)
        del temp['source']
        del temp['target']
        del temp['hop']
        xbmc.log("Receiving XPL ready to copy")
        utils.setSetting("ActivatedFor"+sys.argv[ 1 ], "true")
        utils.setSetting(sys.argv[ 1 ]+"Class", schema)
        utils.setSetting(sys.argv[ 1 ]+"Params", '&'.join("%s=%r" % (key,val) for (key,val) in temp.iteritems()).replace("'", ""))
        xpl.stop()
        dialog.update(num, "Found message !")
        #dialog.close()
        xbmc.log("Window closed")


def Main():
    try:
        #xpl = Xpl(source, xbmc.getIPAddress())
        xpl.parse = parseBroadcast
        xbmc.log("sleeping...")
        
        dialog.create(__addonname__, "Waiting for XPL message...")
        for num in range(0,100):
            xbmc.sleep(50)
            dialog.update(num)
#        xbmc.sleep(5000)
        dialog.close()
        xbmc.log("slept !")
        #xbmc.log("Last class was " + utils.getSetting("InvisibleParameterClass"))
        #xbmc.log("Last parameters were " + utils.getSetting("InvisibleParameterParameter"))
        #xbmc.log(sys.argv[ 1 ]+"Class")
        xpl.stop()
    except Exception, e:
        xbmc.log(__addonname__ + ': ' + str(e), xbmc.LOGERROR)

if (__name__ == "__main__"):
    Main()
